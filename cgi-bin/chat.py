#!/usr/bin/env python3

import cgi

form = cgi.FieldStorage() 

print("Content-Type:text/html\r\n\r\n")

print("""
<html>
<body>
<p>Chat</p>
""")

if "name" in form and "message" in form:
    with open('cgi-bin/data.html', mode='r+', encoding='utf-8') as data_file:
        content=data_file.read()
        data_file.seek(0, 0)
        data_file.write(form["name"].value + ':' + form["message"].value)
        data_file.write('<br /><br />\n' + content)

print("""
<br />

<form action="/cgi-bin/chat.py" method="get">
    Your name: <input type="text" name="name">  <br />
    Message: <textarea rows="4" cols="50" name="message" /></textarea>
    <input type="submit" value="Submit" />
</form>

<br \>
<br \>
""")

with open('cgi-bin/data.html', encoding='utf-8') as data_file:
    print(data_file.read())

print("""
</body>
</html>
""")
