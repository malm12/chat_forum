#!/bin/sh

ip_address=$(ip address | grep '192' | awk '{ print $2}' | sed s:\/.*::g)
port=8000

echo "Location: http://${ip_address}:${port}/"
echo "Stop the server using CTRL+C"

touch "cgi-bin/data.html"
python3 -m http.server --cgi
